from report import Report
from crawler import Crawlers,Kernelgo
class Gipews:
    def __init__(self, Sender) -> None:
        self.report = Report()
        self.sender = Sender()
        self.crawler = Crawlers()
        Kernelgo()
    
    def run(self):
        self.crawler.crawl(self.report)
        self.sender.send(self.report)