from gipews import Gipews
from lark import Lark

if __name__ == '__main__':
    gipews = Gipews(Lark)
    gipews.run()
