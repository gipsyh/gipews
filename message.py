class Message:
    def __init__(self, head, context, href, time) -> None:
        self.head = head.strip()
        self.context = context.strip()
        self.href = href.strip().replace(' ', '%20')
        self.time = time
