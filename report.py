class Report:
    def __init__(self) -> None:
        self.message = []

    def add_message(self, message):
        self.message.append(message)

    def get_message(self):
        return self.message.pop(0)

    def __generate_html(self):
        zh_cn = {
            'title': 'report',
            "content": []
        }
        while True:
            try:
                message = self.get_message()
                print(message.href)
                zh_cn['content'].append([{
                    "tag": "a",
                    "href": message.href,
                    "text": message.head,
                }])
            except:
                break
        html = {
            'zh_cn': zh_cn
        }
        return html

    def generate(self, format):
        if format == 'html':
            return self.__generate_html()
