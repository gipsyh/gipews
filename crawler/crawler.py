from message import Message
_default_header = {
    'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36'
}
class Crawler:
    def __init__(self, url) -> None:
        self.url = url
        self.header = _default_header
    
    def crawl(self, report):
        report.add_message(Message("head", "context", "a", None))
