from message import Message
from .crawler import Crawler
import requests
from bs4 import BeautifulSoup
from datetime import datetime,timezone
class Kernelgo(Crawler):
    def __init__(self) -> None:
        super().__init__('https://kernelgo.org/')
        self.last_time = datetime.now(timezone.utc)

    def crawl(self, report):
        response = requests.get(self.url, self.header)
        if response.status_code != 200:
            return
        soup = BeautifulSoup(response.text, 'lxml')
        articles = soup.find_all('div', class_= 'recent-posts-article')
        for article in articles:
            article_time = datetime.strptime(article.time.attrs['datetime'], '%Y-%m-%dT%H:%M:%S%z')
            if article_time < self.last_time:
                message = Message(article.a.string, article.a.string, article.a.attrs['href'], article_time)
                report.add_message(message)