from .kernelgo import Kernelgo

class Crawlers:
    def __init__(self) -> None:
        self.crawlers = [Kernelgo()]

    def crawl(self, report):
        for crawler in self.crawlers:
            crawler.crawl(report)
